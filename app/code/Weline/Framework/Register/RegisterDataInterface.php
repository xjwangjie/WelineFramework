<?php

/*
 * 本文件由 秋枫雁飞 编写，所有解释权归Aiweline所有。
 * 邮箱：aiweline@qq.com
 * 网址：aiweline.com
 * 论坛：https://bbs.aiweline.com
 */

namespace Weline\Framework\Register;

interface RegisterDataInterface
{
    const NAMESPACE = 'Weline\\Framework\\';

    const MODULE = 'module';

    const I18N = 'i18n';

    const ROUTER = 'router';

    const THEME = 'theme';

    const register_file = 'register.php';
}
