<?php

/*
 * 本文件由 秋枫雁飞 编写，所有解释权归Aiweline所有。
 * 邮箱：aiweline@qq.com
 * 网址：aiweline.com
 * 论坛：https://bbs.aiweline.com
 */

namespace Weline\Framework\View\Data;

class DataInterface
{
    const dir = 'view';

    const view_STATICS_DIR = 'statics';

    const view_TEMPLATE_DIR = 'templates';

    const view_TEMPLATE_COMPILE_DIR = 'tpl';

    const dir_type_TEMPLATE = 'templates';

    const dir_type_STATICS = 'statics';

    const dir_type_TEMPLATE_COMPILE = 'tpl';
}
